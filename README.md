# Barrel-db documentation


## Generate the documentation

Documentation is generated via [mkdocs](http://www.mkdocs.org)

### Install mkdocs

You will need [Python](https://www.python.org/) and [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

    $ virtualenv env
    $ . env/bin/activate
    $ pip install -r requirements.txt

### Render the web site

    $ mkdocs serve

Then open http://localhost:8000

### Build the site

    $ mkdocs build

## Post an issue

Issues should be posted on [barrel-platform dashboard](https://github.com/barrel-db/barrel-platform/issues) with the tag `documentation`
