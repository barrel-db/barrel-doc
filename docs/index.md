# Welcome to Barrel-db!

## Using barrel

This is the official documentation for Barrel-db.

If you are new to Barrel, the [installation procedure](using/install.md) is a
good starting point.

You can then use Barrel with
[HTTP API](using/rest.md), [Python](using/python.md)
or [Elixir](using/elixir.md).

## Join the community

The [Barrel Slack channel](https://slack.barrel-db.org/) is the main point to
meet the community and the core team behind Barrel.

The [forum](https://users.barrel-db.org/) is also available to ask questions and
discuss Barrel related topics.
