# Using Barrel from Python

First, [install Barrel](/using/install.md) on your machine.

We will now create our first micro-services using Python. This library wrap the
[HTTP REST API](/using/rest.md) into easy to use Python functions. It will help us keeping our
code simple and understandable.

Do not forget to install the dependencies libraries:

    $ pip install -r requirements.txt

## Open a database

```python
from barrel import Database

database_url = "http://192.168.1.89:8080/source"
database = Database(database_url)
```

The `database` object can be used for all operations with barrel.

## Read a document

Read the last revision of a document:

```python
from barrel import Database

database = Database(database_url)
doc = database.get(docid)
```

## Create a document

Use the `put` method to create or update a document. Do not forget
to set the mandatory `id` property:

```python
database = Database(database_url)
doc = {'id': 'chat', 'message': 'hello world'}
database.put(doc)
```

You can also use the `post` method if you prefer to let barrel generate
a unique `id`:

```python
database = Database(database_url)
doc = {'message': 'hello world'}
database.post(doc)
```

## Update an existing document

Just use the `put` method to update an existing document. Pay attention
to the `_rev` property of the document which should contain the last known revision.
The easiest way to ensure it, is to read the doc with `get`, update it, and then write
it back with `put`.

```python
database = Database(database_url)
doc = database.get('chat')
doc['message'] = 'hello world'
database.put(doc)
```

## Delete a document

```python
database = Database(database_url)
doc = database.get('chat')
database.delete(doc)
```

## Listening to document update events 

The method `changes(regex)` returns a stream of the documents which have been modified.
The `regex` parameter can be used to filter them on those relevant to you.
 
```python
database = Database(database_url)
for doc in database.changes('^chat'):
    print(doc['message'])
```



