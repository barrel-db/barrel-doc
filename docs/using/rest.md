# HTTP REST API

Barrel-db embed a complete [Swagger](http://swagger.io/) API documentation at `/api-docs`.

This document introduce the main concepts of the HTTP API.

## Document oriented database

Barrel-db is a document oriented database. You can store and retrieve json
documents using the [HTTP](https://tools.ietf.org/html/rfc2616) [REST](http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm) API.
The HTTP verbs `POST`, `PUT`, `GET` and `DELETE` can be used to access documents
as ressources.

The URI of ressources representing documents have the following form:

    /dbs/{db}/docs/{docid}

The `{db}` part identifies the database. A Barrel node running of a specific
port can handle several databases. The `{docid}` part is a unique identifier in
the context of the database.

For instance, you can retrieve the `cat` document within the `source` database
using the HTTP `GET` verb:

    :::js
    GET /dbs/source/docs/cat

You can create a new document using the HTTP `PUT` verb. The json document must
include an `id` property containing the *docid* of the document.

For instance, to create a document at this URI `/dbs/source/docs/cat`:

```js
PUT /dbs/source/docs/cat
{
  'id': 'cat',
  'name': 'Tom'
}
```

You can use the HTTP `POST` verb to create a document without assigning yourself
a *docid*. Barrel-db will compute a unique identifier for you.

```js
POST /dbs/source/docs
{
  "name": "Tom"
}

Reply:
{
  "id": "b0bd800d-58e2-4279-a5fe-c8e21dbe3040",
  "ok": true,
  "rev": "1-4ce4351b36db87b1b455421d9e0be43b"
}
```

## Managing conflicts using revisions

Barrel uses an Optimistic Concurrency Control (OCC) method for concurrent access
to same documents. There is no "transaction" or "lock". The main idea is to
check that nobody updated the document you are going to write between the last
moment you read it, and the moment you write it.


Barrel use the `_rev` property to identfies each unique version of the document.
This property is updated each time the document is written. It is composed of
two parts:

* The first part is a sequential number increased with each update;
* The second part is a MD5 value computed from the content of the document.

Some example values of `_rev`:

```
1-4ce4351b36db87b1b455421d9e0be43b
2-7dce3773ba098f2c09b24697af544e63
3-dce8a2ab14c7e40b60c6550e8af8d0a5
```

When you use a `PUT` or a `DELETE` verb, you must ensure the `_rev` property of
the document contains the last known revision. When updating or deleting the
document, if Barrel observes the revision you provided is not same as on the
database server, this will show that somebody else updated this document and it
will reject your modification.

You should then re-read the document to get the last revision and send again
the request for update or deletion. You can repeat this loop until finally
nobody else modified your document between the moment you read it and the
moment barrel write it.

You might want of course to implement some sort of timeout to avoid your program
to loop forever if your document is heavily updated by multiple other programs.

## HTTP API Reference

A swagger API is available with every barrel instance à rout `/api-docs`.
