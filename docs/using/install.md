# How to install Barrel

## Using Docker

The easiest way to use Barrel is the official Docker image
[available on Docker Hub](https://hub.docker.com/r/barrel/barrel/).

First ensure [Docker is installed](https://docs.docker.com/engine/installation/)
on your machine. Then you can use the standard docker commands to use the Barrel-db
image and create containers. This document provides some examples.

Start Barrel-db as a detached (background) process named *test-barrel*.
You can access the server at URL `http://localhost:7080/`

```text
$ docker run --detach --name=test-barrel -p 7080:7080 barrel/barrel
```

Display the log of the server:

```text
$ docker logs test-barrel
```

Stop the container:

```text
$ docker stop test-barrel
```

Open a `bash` session in the container:

```text
$ docker exec -it test-barrel bash
```

Open a remote console with the Erlang VM:

```text
$ docker exec -it test-barrel /barrel/bin/barrel remote_console
```

## From Source

### Linux Debian/Ubuntu

#### Prerequisites

You need to have [Erlang](http://www.erlang.org/) installed on your machine along the build essential tools.

For installing Erlang, we recommand the [Erlang Solution packages](https://www.erlang-solutions.com/resources/download.html).
The fully packaged `esl-erlang` is working great.

```text
$ wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
$ sudo dpkg -i erlang-solutions_1.0_all.deb
$ sudo apt-get update
$ sudo apt-get install esl-erlang
```

Alternativly, you can use the [standard procedure](https://github.com/erlang/otp/blob/maint/HOWTO/INSTALL.md) provided with Erlang source.
[kerl](https://github.com/kerl/kerl) is a nice bash script which might help the process.


Then you need to install build essential:

```text
$ sudo apt-get install build-essential
```

#### Build Barrel

Follow the procedure provided with Barrel's [source repository](https://github.com/barrel-db/barrel-platform):

```text
$ git clone https://github.com/barrel-db/barrel-platform.git
$ cd barrel-platform
$ make rel
```

*Start the server*

```text
$ ./_build/prod/rel/barrel/bin/barrel_http start
```

*Stop the server*

```text
$ ./_build/prod/rel/barrel/bin/barrel_http stop
```

### MacOS

*Coming soon.*

## Debian/Ubuntu package

*Coming soon.*
