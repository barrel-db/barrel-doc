# Using Barrel from Elixir

You can use the  [`Barrex.Database`](https://github.com/BernardNotarianni/barrel-aof17/blob/master/clients/elixir/lib/barrex.ex) 
library to access a Barrel database from Elixir.

Simply add the `barrex.ex` file to your Elixir project with associated [dependencies](https://github.com/BernardNotarianni/barrel-aof17/blob/master/clients/elixir/mix.exs#L29-L32).

Then re-compile your project

    $ mix deps.get
    $ mix compile

Launch a shell for your project:

    $ iex -S mix

## Open a database

```elixir
database_url "http://192.168.1.89:8080/source"
db = Barrex.Database.open(database_url)
``` 

The `db` object can then be used for all operations on Barrel.

## Read a document

Read the last version of a document:

```elixir
doc = Barrex.Database.get("chat", db)
IO.puts doc["message"]
```
## Create a document

The `put` method creates or updates a document. The `id` property
is mandatory within the document.

```elixir
doc = %{"id" => "chat", "message" => "hello world"}
Barrex.Database.put(doc, db)
```

The `post` method can be used if you want the server to assign by
itself an unique `id`.

```elixir
doc = %{"message" => "hello world"}
Barrex.Database.post(doc, db)
```

## Update an existing document

Use the `put` method to update an existing document.
The `_rev` property must contain the last revision of the document.

```elixir
doc = Barrex.Database.post("chat", db)
doc["message"] = "hello world"
Barrex.Database.put(doc)
```

## Delete a document

```elixir
doc = Barrex.Database.get("chat", db)
Barrex.Database.delete(doc, db)
```

## Streaming updated documents

The `changes(db)` method returns a [stream](https://hexdocs.pm/elixir/Stream.html)
of updated documents:

```elixir
db = Barrex.Database.open(url)
Barrex.Database.changes(db)
  |> Stream.filter(fn(doc) -> doc["id"] == "chat" end)
  |> Stream.map(fn(doc) -> IO.puts doc["message"] end)
  |> Stream.run
```

