# Barrel Node hierarchical resource model and concepts

A barrel node consist of a set of **databases**, each of wich contain **documents** and **related attachments**. 

## Resources

### Wire representation of resource

Barrel does not mandate any proprietary extensions to the JSON standard or special encodings; it works with standard
compliant JSON documents.

### Addressing resources

All resources are URI addressable.


| Uri | Description |
| --- | --- |
| /dbs |Feed of databases on a barrel node |
| /dbs/{dbName} |Database with an id matching the value {dbName} |
| /dbs/{dbName}/docs |Feed of documents under a database |
| /dbs/{dbName}/docs/{docId} |Document with an id matching the value {doc} |
| /dbs/{dbName}/system |Feed of system documents under a database |
| /dbs/{dbName}/system/{docId} |System document with an id matching the value {doc} |
| /replications|Feed of replications tasks running on a node |
| /replications/{replicationId} |Replication task with an id matching the value {replicationId} |


## Databases

A database is container for JSON documents. A database is linked to one storage volume.

## Documents

You can insert, replace, delete, read, enumerate and query arbitrary JSON documents in a database. Barrel does not
mandate any schema and does not require secondary indexes in order to support querying over documents in a collection.   

Being a truly open database service, Barrel does not invent any specialized data types (e.g. date time) or specific
encodings for JSON documents. Note that Barrel does not require any special JSON conventions to codify the
relationships among various documents.

## Attachments and media

A Barrel node allows you to store binary blobs/media either with Barrel or to your own remote media store. It also
allows you to represent the metadata of a media in terms of a special document called attachment. An attachment in
Barrel is a special (JSON) document that references the media/blob stored elsewhere. An attachment is simply a
special document that captures the metadata (e.g. location, author etc.) of a media stored in a remote media storage. 

## System documents

A System document is a document mainy used internally by Barrel to store some states. System documents are not
replicated.

## Replications

The replication is an incremental one way process involving two databases (a source and a destination).

The aim of the replication is that at the end of the process, all active documents on the source database are also in
the destination database and all documents that were deleted in the source databases are also deleted (if exists) on the
destination database.

The replication process only copies the last revision of a document, so all previous revisions that were only on the
source database are not copied to the destination database.
