# Why Barrel?

For decades, organisations have been processing their accounting by hands. This was a
very slow process. Collecting data, analysing and processing them tooks
days and months. Hence, the organisations were able to have a clear and full
understanding of their economic situation only once a year, maybe once every
month for the most efficient ones which hired large teams of clerks.

Then computers arrived and enabled the automation of the whole process.

Despite the fast processing enabled by computer, most companies kept their old
processes and organisation: they produced an image of their economic situation on
a yearly basis, maybe monthly.

And suddently, somebody realised they could do that every day, maybe every hour,
or even in near real time. Those companies used the new technology to its full
potential and then gained the "unfair advantage" to be able to optimise their whole
economic process, fostering better innovation, better services for clients and
more profits.

The purpose of this documentation is to help you to see your business in a
different way and enabling you to redesign it in such a way you could enable
more innovation, improve the quality of your services and hopefully helps you to
reach your business goals.


In the foreword of "Necessary but not sufficient", Eliyahu M. Goldratt
proposes
[6 questions to analyse the power of technology](http://www.goldrattconsulting.com/webfiles/fck/files/The-Power-of-Technology.pdf). Please read the original paper
as what he says is still very relevant to most of the computer technologies you are
using today, and probably tommorow.

Let's see if we could find some specific answers that Barrel bring for us on
the table.

### What is the real power of the Barrel technology?

Barrel enable to connect large numbers of processes, applications, programs,
services, easily, quickly and reliably. This is possible because
Barrel is developped over the Erlang platform which has been setting the standard for
highly scalable and resilient computer systems since 30 years.

Moreover, Barrel use the HTTP REST standard as protocol for communication,
enabling to create efficient connectors for mostly any langages.

At last but not least, Barrel is [reactive](http://www.reactivemanifesto.org/),
meaning you send data accross your network in near real-time condition. This is
yet another core feature of the Erlang platform which has been used for industry
standard messages platforms such as [RabbitMQ](https://www.rabbitmq.com/)
or [ejabberd](https://www.ejabberd.im/).

In summary:

* Barrel is a document oriented database;
* You can connect large numbers of services/applications;
* You can connect your services, whatever the various technologies you used to
  implement them;
* The connections are reactive with near real-time data flows.

### What limitation does this technology diminish?

Connection of computer programs have always been quite complex. Several
standards have emerged accross the history of computer. We can remember Corba or
SOAP were very painfull to implement and use despite lot of advertising and
promises. We also tried the Enterprise Application Integration approach, which
lead to large and complex projects with quite few benefits compared to their cost.

Moreover, **connection of heterogenous systems was complex** and this leaded most
organisations to select very few platforms as a standard (Java is probably the
most well known in this context) and constrainting their engineering teams to use the
standard, whatever they needed to accomplish.


### What rules helped accommodate the limitation?

As the connection of heterogenous system was complex, organisation tends to avoid
to connect systems leading to **monolithic architectures** using standardised
platforms (such as Java)

As applications were monoliths, there evolution rythm was slow: new features were
shipped in large batches, occuring very few time during year. The update
sometime implied to **shutdown the service** while operation teams were proceeding
with installation of the last version of the monolith.

It is still very common to ear people stating rules such as: "don't try to
create micro-services until you can manage your monolith", which actually means:
"**never try to do micro-services**"

As monoliths are quite fragile (any error can crash the whole thing) engineers
became very carefull to modify it. As the size increase, the complexity increase
exponentially leading to even more fragility and requiring to be even more
precautious when updating the system or adding features.

Overall, the implicit rule is **avoid innovation** on our monoliths. 

### What are the rules that should be used now?

Barrel enables to have more **"organic" approach of software**. You can think
of your system as a set of autonomous cells dialoguing each with other.
Some people are using the term of "choregraphy" of micro-services.

Each services could be created by **autonomous teams** or persons, with minimal
communication with other members of the organisation. You could even set an
expiratory date on micro service. The experired services would be recreated with
more mature approach or more efficient technology. This would reduce the risk of
legacy code
and [technical debt](https://martinfowler.com/bliki/TechnicalDebt.html).

Each services could be created with **whatever technology the team decide** to
choose. Could be the one they know best, or the one they think will fit the best,
or maybe the one which look promising. Anyway, if the result is not
satisfactionnary, they could always rewrite it using another technology and
still plug it easily in the whole system.

As the system is a choregraphy of multiple services, potentially redundant, it
will gain a **better resilience to faults**. Moreover, if you allow to have
several services implementing the same feature in different way, you could gain
a **better resilience to bugs**.

### In light of the change in rules what changes are required in the technology?

The good news is that you don't need to rewrite the whole thing upfront. Just
connect your existing system to Barrel and start adding new feature as
micro-services.

Then you can proceed also to rewrite (or move) features from your monolith into
the Barrel micro-services platform. After a while, you could expect to shutdown
the monolith and continuously improve the micro-services systems.


### How to cause the change?

A good starting point is to start using Barrel :-)

You can [install Barrel](/using/install.md) on your machine and connect it with
your prefered langage. If no client library is available, it should be quite
easy to write one over the [HTTP REST API](/using/rest.md).

Then start writing your first micro-services and have them exhange data in
reactive patterns.

Do not hesitate to ask us for help and/or give us feedback of what you have done
:-).
